# Teste de Admissão
O Supermercado Zézinho precisa de um novo sistema para gerir o seu estoque de produtos. Seu papel nesse projeto é desenvolver uma API RESTful que forneça endpoints para gerenciar o cadastro dos produtos e fornecedores.

**instalação**
- Na pasta "appsettings.json", altere os dados da DefaultConnection para seu SQL Server Local. Através de lá referencio o banco de dados.
- Execute pelo console nugget o comando "update-database", caso ter o migrations - se não tiver, crie o migrate com "add-migration NomeDoMigration" e posteriormente execute update-database.
- Utilizei .NET Core 5 e Identity Framework (com suas ramificações) 5.0.3 no VS 2019.

##Aqui está o conograma para auxiliação

###Fornecedor

- ID(int)
- Nome(string)

###Produto

-Id(int)
-Preco(int)
-Descricao(string)
-Estoque(int)
-Fornecedor(int)

**Deixando claro que não consegui realizar todas as tasks**

### tasks não finalizadas

2.5 - Ler um Produto
Forneça um endpoint que permita oter os detalhes de um produto por id. Deve trazer todos os dados do produto.

2.6 - Listar Produtos
Forneça um endpoint que liste todos os produtos, trazendo todos os seus atributos. Além disso, esse endpoint deve conter alguns filtros, que permitem refinar os resultados. Esses filtros podem ser combinados. São eles:

Id
Descrição
Fornecedor
Preço mínimo
Preço máximo

3.3 - Alterar um Fornecedor
Forneça um endpoint que permita editar um fornecedor. As seguintes regras se aplicam:

Id

Não pode ser alterado


Nome

Obrigatório
Mínimo 10 caracteres
Máximo 50 caracteres

3.4 - Deletar um Fornecedor
Forneça um endpoint que permita deletar um fornecedor. As seguintes regras se aplicam:

O fornecedor não pode ser deletado caso ele possua algum produto cadastrado.


3.5 - Ler um Fornecedor
Forneça um endpoint que permita visualizar os detalhes do fornecedor. Ele deve exibir os seguintes detalhes:

Id
Nome
Lista de todos os produtos desse fornecedor


3.6 - Listar fornecedores
Forneça um endpoint que permita listar os fornecedores, mostrando o nome e o id de cada fornecedor.


﻿namespace Classes.Models
{
    public class Produto
    {
        public int IdProduto { get; set; }
        public string Descricao { get; set; }
        public float Preco { get; set; }
        public int Estoque { get; set; }
        public int Fornecedor { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Classes.Models;
using MercadoZezinhoAPI.Data;

namespace MercadoZezinhoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FornecedoresController : ControllerBase
    {
        private readonly Context _context;

        public FornecedoresController(Context context)
        {
            _context = context;
        }

        // GET: api/Fornecedores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Fornecedor>>> Fornecedor()
        {
            return await _context.Fornecedores.ToListAsync();
        }

        // GET: api/Fornecedor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Fornecedor>> Fornecedor(int id)
        {
            var fornecedor = await _context.Fornecedores.FindAsync(id);
            var produtos = await _context.Produtos.ToListAsync();
            if (fornecedor == null)
            {
                return NotFound();
            }

            return fornecedor;

        }

        // PUT: api/Fornecedores/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> AtualizarFornecedor(int id, Fornecedor fornecedor)
        {
            if (id != fornecedor.IdFornecedor)
            {
                return BadRequest();
            }

            _context.Entry(fornecedor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FornecedorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fornecedores
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Fornecedor>> InserirFornecedor(Fornecedor fornecedor)
        {

            if (fornecedor.NomeFornecedor.Length < 10 && fornecedor.NomeFornecedor.Length < 50){
                return BadRequest("O campo NomeFornecedor deve ter entre 10 à 50 caracteres.");
            }
          
            _context.Fornecedores.Add(fornecedor);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // DELETE: api/Fornecedor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Produto>> DeletarFornecedor(int id, Produto produto)
        {
            
            var fornecedor = await _context.Fornecedores.FindAsync(id);

            var listaProdutos = await _context.Produtos.FindAsync(id);

            //if(listaProdutos)
            //{
            //    return BadRequest("Não foi possível excluir fornecedor! Há produtos vinculados ao fornecedor. Altere e tente novamente.");
            //}

            if (fornecedor == null)
            {
                return NotFound();
            }

            _context.Fornecedores.Remove(fornecedor);
            await _context.SaveChangesAsync();

            return listaProdutos;

        }

        private bool FornecedorExists(int id)
        {
            return _context.Fornecedores.Any(e => e.IdFornecedor == id);
        }
    }
}

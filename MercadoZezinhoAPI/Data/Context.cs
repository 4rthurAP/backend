﻿using Classes.Models;
using Microsoft.EntityFrameworkCore;

namespace MercadoZezinhoAPI.Data
{
    public class Context : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }

        public DbSet<Fornecedor> Fornecedores { get; set; }


        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Produto>
            (etd =>
            {
                etd.ToTable("TabelaProdutos");
                etd.HasKey(p => p.IdProduto).HasName("IdProduto");
                etd.Property(p => p.IdProduto).HasColumnType("int").ValueGeneratedOnAdd();
                etd.Property(p => p.Descricao).IsRequired().HasMaxLength(300);
                etd.Property(p => p.Preco).IsRequired().HasColumnType("float");
                etd.Property(p => p.Estoque).IsRequired();
                etd.Property(p => p.Fornecedor).IsRequired();
            }
            );

            modelBuilder.Entity<Fornecedor>
            (etd =>
            {
                etd.ToTable("TabelaFornecedores");
                etd.HasKey(f => f.IdFornecedor).HasName("IdFornecedor");
                etd.Property(f => f.IdFornecedor).HasColumnType("int").ValueGeneratedOnAdd();
                etd.Property(f => f.NomeFornecedor).IsRequired().HasMaxLength(50);
            }
            );
        }
    }
}
